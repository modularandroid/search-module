# Search Module

A configurable and customizable search module for android.



## Installation

From Android Studio, import the library module to your project :

1. Click File > New > Import Module

2. Choose **search-module** directory and click Finish. 
     *In case of any import error, just proceed to step 3 and the error should be automatically solved.*

3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

	```
	include ":app", ":search-module"   
	```

4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

	```
	dependencies {
		implementation project(":search-module")
	}
	```

5. Click Sync Project with Gradle Files (Sync Now)

6. In your **app AndroidManifest.xml**,  under application, add this line : 

	```
	<application
		...>
		<activity android:name="com.mecreativestudio.search_module.SearchModule"/>
	</application>
	```



## Usage

In the activity in which you want to call the module, import it by adding the following line :

```JAVA
import com.mecreativestudio.search_module.SearchModule;
```



You can call the module with this simple intent :

```java
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, SearchModule.class);
startActivity(intent);
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module.*



### Example

Let's say you have an activity called ***MainActivity*** in which a ***searchButton*** button is implemented. You want your search module to be launched after clicking this button.

By following the previous instructions, the code which you should have to do so is the following : 

```java
import ...
import com.mecreativestudio.search_module.SearchModule;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         //Add a click listener on the searchButton
        Button searchButton = findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the search-module
                Intent intent = new Intent(MainActivity.this, SearchModule.class);
                startActivity(intent);
            }
        });
    }
}
```



## Configuration and Customization

This Search Module is fully configurable and customizable.  



When you will want to use this module, you will notice that it is currently configured to work with this API URL :  https://jsonplaceholder.typicode.com/users

If you want to adapt its behavior to your needs, you can simply edit the SearchModule and SearchAdapter Java classes which are located here :

```
YOUR_PROJECT/search-module/java/com.mecreativestudio.search_module/SearchModule.java
YOUR_PROJECT/search-module/java/com.mecreativestudio.search_module/SearchAdapter.java
```



#### Guideline	

1. In the SearchModule Java Class : 

    - Below the line 32, adapt the 4 variables (which include the API URL) to your case

    - Below the line 86, adapt the getData() and setData() methods to your case

    - Below the line 159, adapt the initArrayLists() and clearArrayLists methos to your case

      

2.   Adapt the SearchAdapter Java class to your case and according to the previous changes





If you want to edit its layout, you can simply edit the search_layout.xml file which is located here :

```xml
YOUR_PROJECT/search-module/res/layout/search_layout.xml
```

If you want to edit the layout of a single item, you can simply edit the search_result_item.xml file which is located here :

```
YOUR_PROJECT/search-module/res/layout/search_result_item.xml
```

*If you modify its layout, make sure there is an overall consistency with the SearchModule and SearchAdapter Java classes.*